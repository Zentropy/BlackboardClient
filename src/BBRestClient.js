import PropTypes from "prop-types";
//const PropTypes = require("prop-types");
import request from "request-promise";
import Membership from "./BBRest/Membership.js";
//const request = require("request-promise");
require("request-debug")(request);

class RestClient {
	
	constructor(origin, key, secret) {
		this.key = key;
		this.secret = secret;
		this.auth = "Basic " + new Buffer(this.key + ":" + this.secret).toString("base64");
		this.origin = origin;
		this.token = "A TOKEN";
	}
	
	Initialize = async () => {
		await this.GetToken();
		//console.log("got token promise fulfilled: " + this.token);
		return this.token; //returning for basically no reason other than debug display
	};
	
	GetToken = async () => {
		const options = {
			method: "post",
			url: this.origin + "/learn/api/public/v1/oauth2/token",
			headers: { Authorization: this.auth },
			form: { grant_type: "client_credentials" },
			json: true
		};
		
		let thisbind = this;
		return request(options)
			.then(function(parsedBody) {
				thisbind.token = parsedBody.access_token;
				//console.log("Setting access token: ", thisbind.token);
			})
			.catch(function(err) {
				console.log(JSON.stringify(err));
				console.error("REQUEST ERROR!");
			});
	};
	
	ExecuteRequest = async (method, endpoint, data) => {
		const execute = this;
		const options = {
			method: method,
			url: encodeURI(this.origin + "/learn/api/public/v1/" + endpoint.replace(/^\//, "")),
			headers: {
				Authorization: "Bearer " + execute.token,
				"content-type": "application/json"
			},
			body: data,
			json: true,
			resolveWithFullResponse: true
		};
		
		return request(options)
			.then(function(response) {
				return response.body;
			})
			.catch(function(err) {
				console.log("Request failed with err: ", err.message);
			});
	};
	
	GetAllUsers = async () => {
		const endpoint = "/users";
		console.log("Getting all users...");
		return await this.ExecuteRequest("get", endpoint, null);
	};
	
	GetUser = async (UserName) => {
		const endpoint = "/users/userName:" + UserName;
		return await this.ExecuteRequest("get", endpoint, null);
	};
	
	CreateUser = async (user) => {
		const endpoint = "/users";
		console.log("Creating user: " + user.userName);
		
		return await this.ExecuteRequest("post", endpoint, user);
	};
	
	GetDataSources = async () => {
		const endpoint = "/dataSources";
		console.log ("Getting data sources");
		return await this.ExecuteRequest("get", endpoint, null);
	};
	
	CreateMultipleCoursesEnrollments = async (UserName, CourseList, IsInstructor, dskId = '_2_1') => {
			for (let courses of CourseList) {
				let enroll = await this.CreateEnrollment(UserName, courses, IsInstructor, dskId);
				console.log("Enrolled in: " +courses);
			}
	};
	
	CreateMultipleEnrollmentsInOneCourse = async (UserNameList, CourseID, IsInstructor, dskId = '_2_1') => {
		for (let user of UserNameList) {
			let enroll = await this.CreateEnrollment(user, CourseID, IsInstructor, dskId);
			console.log("Enrolled: " + user);
		}
	};
	
	CreateEnrollment = async (UserName, CourseID, IsInstructor, dskId = '_2_1') => {
		//dskId is normally "_2_1" here
		const endpoint = "/courses/courseId:" + CourseID + "/users/userName:" + UserName;
		console.log("Endpoint is: " + endpoint);
		
		let membership = new Membership();
		membership.dataSourceId = dskId;
		//membership.userId = UserName;
		//membership.courseId = CourseID;
		if (IsInstructor) {
			membership.courseRoleId = "Instructor";
		}
		
		return await this.ExecuteRequest("put", endpoint, membership.toJson());
	};
	
	ReadCourse = async(CourseID) => {
		const endpoint = "/courses/courseId:" + CourseID;
		return await this.ExecuteRequest("get", endpoint, null);
	};
}

RestClient.propTypes = {
	key: PropTypes.string,
	secret: PropTypes.string,
	auth: PropTypes.string,
	origin: PropTypes.string,
	token: PropTypes.string
};

export default RestClient;

