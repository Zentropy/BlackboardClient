class Membership {
	
	//userId = '';
	//courseId = '';
	dataSourceId = '';
	availability = {
		available: "Yes"
	};
	courseRoleId = 'Student';
	
	toJson() {
		let jsonedObject = {};
		for (let x in this) {
			if (x === "toJSON" || x === "constructor") {
				continue;
			}
			
			jsonedObject[x] = this[x];
		}
		return jsonedObject;
	}
}

export default Membership;
