class User {
	
	externalId = "";
	dataSourceId = "_451_1"; //DSK specific to REST API
	userName = "";
	password = "";
	studentId = "";
	birthDate = "";
	
	//lastLogin = "";
	availability = {
		available: "Yes"
	};
	name = {
		given: "",
		family: "",
		// middle: "",
		// other: "",
		// suffix: ""
	};
	job = {
		title: "",
		department: "",
		//company: ""
	};
	contact = {
		homePhone: "",
		mobilePhone: "",
		businessPhone: "",
		businessFax: "",
		email: "",
		webPage: ""
	};
	address = {
		street1: "",
		street2: "",
		city: "",
		state: "",
		zipCode: "",
		country: ""
	};
	locale = {
		id: "en-us",
		calendar: "Gregorian",
		firstDayOfWeek: "Sunday"
	};
	
	toJSON() {
		let jsonedObject = {};
		for (let x in this) {
			if (x === "toJSON" || x === "constructor") {
				continue;
			}
			
			jsonedObject[x] = this[x];
		}
		return jsonedObject;
	}
}

export default User;