import Rebase from 're-base';
import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyBdPLv_z2URc1afAzRBIyN9erkHT8vXt64",
  authDomain: "catch-of-the-day-33e74.firebaseapp.com",
  databaseURL: "https://catch-of-the-day-33e74.firebaseio.com"
});

const base = Rebase.createClass(firebaseApp.database());

//named export
export {firebaseApp};

//this is default export
export default base;