import React from "react";
import { render } from "react-dom";
import "./css/style.css";
import MainRouter from "./components/MainRouter.js";

require('dotenv').config();



render(<MainRouter />, document.querySelector("#main"));
