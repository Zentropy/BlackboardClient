import React from "react";
import Header from "./Header.js";
import Order from "./Order.js";
import Inventory from "./Inventory.js";
import sampleFishes from "../sample-fishes.js";
import Fish from "./Fish.js";
import base from "../base";
import BBRestComponent from "./BBRestComponent.js";


class App extends React.Component {

  state = {
    fishes: {},
    order: {},
    bbrest: {}
  };

  addFish = (fish) => {
    console.log("Adding fish");
    //for updating state - take a copy of the existing state (state is immutable)
    const fishesCopy = { ...this.state.fishes };
    //add new fish to the copy
    fishesCopy[`fish${Date.now()}`] = fish;
    //set new fishes object to state
    this.setState({
      fishes: fishesCopy
      //can do just `fishes` alone if you rename fishesCopy to where it matches
    });
  };

  loadSampleFishes = () => {
    this.setState({
      fishes: sampleFishes
    });
  };

  addToOrder = (key) => {
    const order = { ...this.state.order };
    order[key] = order[key] + 1 || 1;
    this.setState({ order });

  };

  setBBRestState = (newState) => {
    console.log("Setting bb rest state");
    this.setState({bbrest: newState});
  };

  componentDidMount() {
    const storeID = this.props.match.params.storeId + "/fishes";

    //first reinstate localstorage
    const localStorageRef = localStorage.getItem(this.props.match.params.storeId);
    const loadOrder = JSON.parse(localStorageRef);
    if (localStorageRef) {
      this.setState({order: loadOrder});
    }

    //stick fish inventory into firebase
    //this.ref = base.syncState(`${storeID}` + "/fishes", {
    this.ref = base.syncState(`${storeID}`, {
      context: this,
      state: 'fishes'
    });
  }

  componentDidUpdate() {
    //add new item to local storage for order
    // eslint-disable-next-line no-useless-concat
    console.log("Updated!" + " " + this.props.match.params.storeId + " - " + this.state.order);
    localStorage.setItem(this.props.match.params.storeId, JSON.stringify(this.state.order));
  }

  componentWillUnmount() {
    console.log("Unmounting");
    //need to clean up the listening, which is why we stored this.ref to begin with
    base.removeBinding(this.ref);
  }

  render() {
    return (

      <div className="catch-of-the-day">
        <div className="menu">
          <Header tagline="Fresh Seafood Market"/>
          <ul className={"fishes"}>
            {Object.keys(this.state.fishes).map(key => (
              <Fish
                key={key}
                index={key}
                addToOrder={this.addToOrder}
                details={this.state.fishes[key]}
              />
            ))}
          </ul>
        </div>
        <Order fishes={this.state.fishes} order={this.state.order}/>
        <Inventory addFish={this.addFish} loadSampleFishes={this.loadSampleFishes} />
        <BBRestComponent state={this.state.bbrest} setBBRestState={this.setBBRestState}/>
      </div>

    );
  }
}

export default App;
