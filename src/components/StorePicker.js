import React from "react";
import { getFunName } from "../helpers.js";

class StorePicker extends React.Component {

  // constructor() {
  //   super();
  //   //this.goToStore = this.goToStore.bind(this);
  //   //console.log(this.storeInput);
  // }

  storeInput = React.createRef();

  goToStore = (event) => {
    event.preventDefault(); // stop form from submitting
    //get text from input
    const storeName = this.storeInput.current.value;
    //change the page to /store/whatever-they-entered
    this.props.history.push(`/store/${storeName}`)
  };

  // componentDidMount() {
  //   console.log(this.storeInput);
  // }

  render() {
    return (
      <form className={"store-selector"} onSubmit={this.goToStore}>
        {/* This is html comment for JSX */}
        <h2>Please Enter A Store:</h2>
        <input
          type={"text"}
          // ref={this.storeInput}
          ref={this.storeInput}
          required
          placeholder={"Store Name"}
          defaultValue={getFunName()}
        />
        <button type={"submit"}>Visit Store </button>
      </form>
    );
  }
}

export default StorePicker;
