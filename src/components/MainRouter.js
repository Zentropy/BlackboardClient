import { BrowserRouter, Route, Switch } from "react-router-dom";
import StorePicker from "./StorePicker.js";
import React from "react";
import App from "./App.js";
import NotFound from "./NotFound.js";
import Redirect from "react-router-dom/es/Redirect.js";
import BlackboardLandingPage from "./BlackboardLandingPage.js";

const MainRouter = () => (
  <BrowserRouter>
    <Switch>

      {/*<Route exact path="/" component={App} />*/}
      {/*<Route exact path="/" component={DebugStore} />*/}
      <Route exact path={"/kcstore"} component={() => {
        return (<Redirect to={"/store/kcstore"} />)
      }} />

      <Route exact path={'/'} component={BlackboardLandingPage} />
      <Route path="/picker" component={StorePicker} />
      <Route path="/store/:storeId" component={App} />
      <Route component={NotFound} />
    </Switch>
  </BrowserRouter>
);

export default MainRouter;
