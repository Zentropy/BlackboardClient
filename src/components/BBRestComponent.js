import React from "react";
import  RestClient  from "../BBRestClient.js";

class BBRestComponent extends React.Component {
	
	success = false;
	
	constructor() {
		super();
		
		var origin = "https://example.blackboard.com";
		var key = "a8bcfd83-da42-4c42-a156-570ab06e4099";
		var secret = "g9p5UIr1gtPNXHoj4W55gdemPwN1EqrX";
		
		const origin_dev = "https://cors-anywhere.herokuapp.com/https://training.pulaskitech.blackboard.com";
		const origin_production = "https://cors-anywhere.herokuapp.com/https://elearning.pulaskitech.edu";
		const USE_DEV_MODE = false;
		
		if (USE_DEV_MODE) {
			origin = origin_dev;
			
		} else {
			origin = origin_production;
		}
		
		this.restApp = new RestClient(origin, key, secret);
		
		//this.goToStore = this.goToStore.bind(this);
		this.TryRestApp = this.TryRestApp.bind(this);
		//console.log(this.storeInput);
	}
	
	async TryRestApp() {
		if (!this.success) {
			//$user = $this->getUsersFromPage('/learn/api/public/v1/users?offset=0', $access_token);
			const self = this;
			
			this.restApp.get("/users?offset=0", {
				data: {},
				complete: function(error, response, body) {
					//console.log("error:", error);
					//console.log("statusCode:", response && response.statusCode);
					//console.log("body:", body);
					self.body = body;
					if (response.statusCode === 200) {
						console.log("Success Authenticating!");
						//console.log (self.body.results[0]);
						self.success = true;
						let p = self.props.state;
						console.log("preparing to set state to: ");
						console.log(self.body);
						self.props.setBBRestState(self.body);
						console.log("Finihed setting state: " + p);
					}
					else {
						console.log("Failure, status code is: " + response.statusCode);
						self.success = false;
					}
				}
			});
		}
	};
	
	componentDidMount() {
		this.TryRestApp();
	}
	
	renderState = () => {
		if (typeof this.props.state.paging !== "undefined") {
			return (
				<div>
					{this.props.state.paging.nextPage}
				</div>
			);
		} else {
			return "NOTHING";
		}
	};
	
	render() {
		return (
			<div className={"bbrest"}>
				{this.renderState()}
			</div>
		);
		
	}
}

export default BBRestComponent;
