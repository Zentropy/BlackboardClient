import React from "react";

class AddUserForm extends React.Component {
	
	firstNameRef =  React.createRef();
	lastNameRef = React.createRef();
	emailRef = React.createRef();
	studentIdRef = React.createRef();
	
	
	createUser = (event) => {
		event.preventDefault();
		
		// const fish = {
		// 	name: this.nameRef.current.value,
		// 	price: parseFloat(this.priceRef.current.value),
		// 	status: this.statusRef.current.value,
		// 	desc: this.descRef.current.value,
		// 	image: this.imageRef.current.value,
		// };
		
		//userName, firstName, lastName, email, studentId
		let username = this.emailRef.current.value;
		let firstname = this.firstNameRef.current.value;
		let lastname = this.lastNameRef.current.value;
		let email = this.emailRef.current.value;
		this.props.createUserCB(username, firstname, lastname, email);
		
		//reset form
		event.currentTarget.reset();
	};
	
	render() {
		return (
			
			<form className={"fish-edit"} onSubmit={this.createUser}>
				<input name="firstname" ref={ this.firstNameRef }  type="text" placeholder="First Name" />
				<input name="lastname" ref={ this.lastNameRef }  type="text" placeholder="Last Name" />
				<input name="email" ref={this.emailRef} type="text" placeholder={"Email Address"} />
				<input name="studentId" ref={this.studentIdRef} type={"text"} placeholder={"Student ID"} />
				
				<button type={"submit"}>+ Add User</button>
			</form>
		
		
		
		);
	}
}

export default AddUserForm;
