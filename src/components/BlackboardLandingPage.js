import React, { Component } from "react";
import RestClient from "../BBRestClient.js";
import User from "../BBRest/User.js";
import AddUserForm from "./AddUserForm.js";
//const sql = require('mssql');
//var Connection = require('tedious').Connection;
const generatePassword = require('password-generator');

class BlackboardLandingPage extends Component {
	
	state = {};
	
	constructor(props) {
		super(props);
		let origin = "https://example.blackboard.com";
		let key = "a8bcfd83-da42-4c42-a156-570ab06e4099";
		let secret = "g9p5UIr1gtPNXHoj4W55gdemPwN1EqrX";
		
		const origin_dev = "https://cors-anywhere.herokuapp.com/https://training.pulaskitech.blackboard.com";
		const origin_production = "https://cors-anywhere.herokuapp.com/https://elearning.pulaskitech.edu";
		const USE_DEV_MODE = false;
		
		if (USE_DEV_MODE) {
			origin = origin_dev;
		} else {
			origin = origin_production;
		}
		
		
		
		//Initialize REST client
		this.restApp = new RestClient(origin, key, secret);
		//Initialize auth token
		
	}
	
	handlePromise = () => {
		console.log("REST APP: " + JSON.stringify(this.restApp));
		this.setState({ state: JSON.stringify(this.restApp) });
	};
	
	testSQL = async () => {
		//jdbc:sqlserver://Bb-EX-Link;database=Bb_Interface
		const config = {
			server: 'Bb-EX-Link',
			database: 'Bb_Interface',
			userName: 'Bbinterface',
			password: process.env.REACT_APP_MSSQL_PASSWORD,
			port: 1433
		};
	};
	
	componentDidMount() {
		console.log("MOUNTED");
		this.restApp.Initialize();
	}
	
	createUser = async (userName, firstName, lastName, email, studentId) => {
		//Testing create user
		console.log("Creating new user in CB");
		let newuser =  new User();
		newuser.externalId = email;
		newuser.availability.available = 'Yes';
		newuser.name.given = firstName;
		newuser.name.family = lastName;
		newuser.userName = email;
		newuser.contact.email = email;
		newuser.studentId = studentId;
		newuser.password = generatePassword(12, false);
		let newbie = await this.restApp.CreateUser(newuser.toJSON());
		console.log("Created new account for: " + newuser.userName);
		this.setState({ state: JSON.stringify(newbie)});
	};
	
	
	handleClick = async (event) => {
		//this.testSQL();
		
		
		
		
		//testing read course
		//let readcourse = await this.restApp.ReadCourse('BUS1403.62.FA18');
		//console.log("Got course read:");
		//console.dir(readcourse);
		
		//Testing enroll instructor
		//let enrollment = await this.restApp.CreateEnrollment('jprice@uaptc.edu', 'BUS1403.63.FA18', true);
		//console.log("Enrollment:");
		//console.dir(enrollment);
		
		//Testing multiple enrollment
		//let courselist = ['SPAN1311.01.FA18', 'SPAN1311.03.FA18', 'SPAN1311.61.FA18', 'SPAN1312.01.FA18','SPAN2311.01.FA18','SPAN2311.60.FA18'];
		//let mult = await this.restApp.CreateMultipleCoursesEnrollments('lchangshik@uaptc.edu', courselist, true);
		
		//Testing GetAllUsers
		//let users = await this.restApp.GetAllUsers();
		//console.log("Got users:");
		//console.log(users);
		//this.setState({ state: JSON.stringify(users) });
		
		//Testing GetDatasources
		//let datasources = await this.restApp.GetDataSources();
		//console.log("Got data sources");
		//console.log(datasources);
		//this.setState({ state: JSON.stringify(datasources)});
		
		
		
	};
	
	render() {
		return (
			<div>
				<button onClick={this.handleClick}>Load</button>
				<AddUserForm createUserCB={this.createUser}/>
				<br/>
				{JSON.stringify(this.state)}
			</div>
		);
	}
}

export default BlackboardLandingPage;
