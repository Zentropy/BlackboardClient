import React from "react";
import PropTypes from "prop-types";

//same as: function Header(props) { return <header....}
//avoids using a whole ReactComponent just for a plain render message
const Header = props => (
  <header className={"top"}>
    <h1>
      Catch
      <span className="ofThe">
        <span className="of">Of</span>
        <span className="the">The</span>
      </span>
      Day
    </h1>
    <h3 className="tagline">
      <span>{props.tagline}</span>
    </h3>
  </header>
);

// class Header extends React.Component {
//   render() {
//     return (
//
//     );
//   }
// }

Header.propTypes = {
  tagline: PropTypes.string
};

export default Header;
